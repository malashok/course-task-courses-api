import {
  BadRequestException,
  Controller,
  Get,
  InternalServerErrorException,
  NotFoundException,
  Param
} from '@nestjs/common';
import { ScheduleService } from './schedule.service';

@Controller('schedule')
export class ScheduleController {
  constructor(
    protected readonly service: ScheduleService,
  ) {}

  @Get(':year/:season')
  public async getSchedule(@Param('year') year: number, @Param('season') season: number): Promise<unknown> {
    require('https').globalAgent.options.rejectUnauthorized = false;
    return this.service.getSchedule(year, season).catch(err => {
      const errorCode = err?.response?.status;
      if (errorCode == 404) {
        throw new NotFoundException('Not Found');
      } else if (errorCode == 400) {
        throw new BadRequestException('Bad Request');
      } else if (errorCode == 500) {
        throw new InternalServerErrorException('Internal Server Error');
      }
    });
  }
}
