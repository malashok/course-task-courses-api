import {Injectable} from '@nestjs/common';
import axios from "axios";
import * as cheerio from 'cheerio';
import {CourseSeason, EducationLevel} from "../../common/types";
import {IScheduleItem} from "./types";

@Injectable()
export class ScheduleService {
    public async getSchedule(year: number, season: number): Promise<IScheduleItem[]> {
        if(season != 1) year--;
        const {data} = await axios.get(`https://my.ukma.edu.ua/schedule/?year=${year}&season=${season}`);
        const $ = cheerio.load(data);
        let arr: IScheduleItem[] = [];
        let courseSeason: CourseSeason;
        if (season == 1) {
            courseSeason = CourseSeason.AUTUMN;
        } else if (season == 2) {
            courseSeason = CourseSeason.SPRING;
        } else {
            courseSeason = CourseSeason.SUMMER
        }

        const faculties = $('#schedule-accordion .panel-info');


        faculties.each((n, el) => {
            let faculty = $(el).find('.panel-heading > h4 > a').first().text().trim();
            let data = $(el).find('.panel-collapse > div > div').children();

            data.each((n, item) => {
                let levelEl = $(item).find('div > h4 > a').first().text().trim().split(',')[0];
                let level: EducationLevel;
                if (levelEl == 'БП') {
                    level = EducationLevel.BACHELOR;
                } else {
                    level = EducationLevel.MASTER;
                }

                let otherData = $(item).find('.panel-collapse > .list-group').children();

                otherData.each((n, elem) => {
                    let url = String($(elem).find('div > a').last().attr('href'));
                    let t = $(elem).find('div > a').last().text().trim();
                    let specName = t.split(levelEl)[0].slice(0, -1);
                    let tmp = $(elem).find('div > span').first().text().trim().split(' ');
                    let day = tmp[1].split('.');
                    let time = tmp[2].slice(0, -1);
                    let updatedAt = `${day[2]}-${day[1]}-${day[0]} ${time}`;

                    arr.push({
                        url: url,
                        updatedAt: updatedAt,
                        facultyName: faculty,
                        specialityName: specName,
                        level: level,
                        year: year,
                        season: courseSeason,
                    });
                });
            });
        });
        return arr;
    }
}
