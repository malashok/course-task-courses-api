import {Injectable} from '@nestjs/common';
import {ICourse} from './types';
import axios from "axios";
import * as cheerio from 'cheerio';
import {CourseSeason, EducationLevel} from '../../common/types';


@Injectable()
export class CourseService {
    public async getCourse(code: number): Promise<ICourse> {
        const {data} = await axios.get(`https://my.ukma.edu.ua/course/${code}`);
        const $ = cheerio.load(data);
        const course: number = Number($("#w0 > table > tbody:nth-child(1) > tr:nth-child(1) > td").text().trim());
        const courseName = $(".page-header > h1").text().trim().split('\t\t')[0];
        const description = $("#course-card--" + code + "--info").text().replace('\n\t\t\t\t\t', '').replace('\t\t\t\t', '');
        const facultyName = $("#w0 > table > tbody:nth-child(1) > tr:nth-child(3) > td").text();
        const departmentName = $("#w0 > table > tbody:nth-child(1) > tr:nth-child(4) > td").text();
        const edLevel = $("#w0 > table > tbody:nth-child(1) > tr:nth-child(5) > td").text();
        let level;
        if (edLevel == "Бакалавр") {
            level = EducationLevel.BACHELOR;
        } else {
            level = EducationLevel.MASTER;
        }

        const year = Number($("#w0 > table > tbody:nth-child(1) > tr:nth-child(2) > td > span:nth-child(3)")
            .text().trim().split(' ')[0]);

        const listOfSeasons = $("#w0 > table > tbody:nth-child(2)");
        const seasons: CourseSeason[] = [];
        listOfSeasons.each((n, el) => {
            const text = $(el).find("th").text().trim().split('\t');
            text.forEach(t => {
                if (t == 'Осінь') {
                    seasons.push(CourseSeason.SPRING);
                } else if (t == 'Весна') {
                    seasons.push(CourseSeason.AUTUMN);
                } else if (t == 'Літо'){
                    seasons.push(CourseSeason.SUMMER);
                }
            });
        });
        const credits: number = Number($("#w0 > table > tbody:nth-child(1) > tr:nth-child(2) > td > span:nth-child(1)")
            .text().trim().split(' ')[0]);
        const hours: number = Number($("#w0 > table > tbody:nth-child(1) > tr:nth-child(2) > td > span:nth-child(2)")
            .text()[0]);

        const teacherName = $("#w0 > table > tbody:nth-child(1) > tr:nth-child(7) > th").next().text();
        return {
            code: course,
            name: courseName,
            description: description,
            facultyName: facultyName,
            departmentName: departmentName,
            level: level,
            year: year,
            seasons: seasons,
            creditsAmount: credits,
            hoursAmount: hours,
            teacherName: teacherName
        };

    }

}
