import {
  BadRequestException,
  Controller,
  Get,
  InternalServerErrorException,
  NotFoundException,
  Param
} from '@nestjs/common';
import { CourseService } from './course.service';

@Controller('course')
export class CourseController {
  constructor(
    protected readonly service: CourseService,
  ) {}

  @Get(':code')
  public async getCourse(@Param('code') code: number): Promise<unknown> {
    require('https').globalAgent.options.rejectUnauthorized = false;
    return this.service.getCourse(code).catch(err => {
      const errorCode = err?.response?.status;
      if (errorCode == 404) {
        throw new NotFoundException('Not Found');
      }else if (errorCode == 400){
        throw new BadRequestException('Bad Request');
      }else if (errorCode == 500){
        throw new InternalServerErrorException('Internal Server Error');
      }
    });

  }
}
